<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Http;

trait ApiTrait
{

    private $baseUrl = 'https://www.breakingbadapi.com/api/';


    public function getDeathInfo($name)
    {

        $response = Http::get($this->baseUrl . 'death?name=' . $name);
        if ($response->successful()) {
            return json_decode($response->body());
        } else {
            // Error handling done here
            return null;
        }
    }

    public function getQuotes($name)
    {

        $response = Http::get($this->baseUrl . 'quote?author=' . $name);
        if ($response->successful()) {
            return json_decode($response->body());
        } else {
            //Error handling done here
            return null;
        }
    }
}
