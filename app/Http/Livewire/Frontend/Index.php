<?php

namespace App\Http\Livewire\Frontend;

use App\Http\Livewire\Base\BaseComponent;
use App\Models\Character;
use Livewire\Component;

class Index extends BaseComponent
{

//    public $search;

    public array $filters = [
        'search' => '',
        'season' => '',
        'category' => 'Breaking Bad'

    ];

    public function mount()
    {
        $this->getRecordsQueryProperty();
    }

    public function clearFilters()
    {
        $this->filters['search'] = '';
        $this->filters['season'] = '';
        $this->filters['category'] = '';
    }

    public function getRecordsQueryProperty()
    {

        $category = $this->filters['category'];
        $season = $this->filters['season'];

        $query = Character::query()
            ->when($this->filters['search'], fn($query, $search) => $query->where('name', 'iLike', '%' . $search . '%'))
            ->with('appearances', 'occupations', 'categories');

        if ($category) {
            $query = $query->whereHas('categories', function ($q) use ($category) {
                $q->where('category', $category);
            });
        }

        if ($season) {
            $query = $query->whereHas('appearances', function ($q) use ($season) {
                $q->where('episode', $season);
            });
        }


        return $this->applySorting($query);

    }


    public function render()
    {
        return view('livewire.frontend.index', ['characters' => $this->records])->extends('welcome')->section('mainBody');
    }
}
