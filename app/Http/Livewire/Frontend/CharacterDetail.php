<?php

namespace App\Http\Livewire\Frontend;

use App\Http\Livewire\Base\BaseComponent;
use App\Http\Traits\ApiTrait;
use App\Models\Character;
use Livewire\Component;

class CharacterDetail extends BaseComponent
{
    use ApiTrait;

    public $record;
    public $deathInfo;
    protected $rules = [
        'record.name' => 'required',
        'record.nickname' => 'required',
        'record.status' => 'required',
    ];

    public function mount($id)
    {

        $this->record = Character::with('categories', 'appearances', 'occupations')
            ->findOrFail($id);

        $characterName = urlencode($this->record->name);

        if ($this->record->status != "Alive") {
            $this->deathInfo = $this->getDeathInfo($characterName);
        }

    }

    public function edit($id)
    {

        $this->record = Character::where('id', $id)->first();
//        if ($this->record->isNot($obj)) $this->record = $obj; //fields are preserved when editing

        $this->showEditModal = true;
    }

    public function save()
    {
        $this->validate();
        $this->record->save();

        $this->showEditModal = false;

//        $this->dispatchBrowserEvent('notify', 'Character Updated!');

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Character Updated!',
//            'text' => 'Expect a phone call to confirm order.',
        ]);
        $this->saved = true;

    }


    public function render()
    {
        return view('livewire.frontend.character-detail', [
            'quotes' => $this->getQuotes(urlencode($this->record->name))

        ])->extends('welcome')->section('mainBody');
    }
}
