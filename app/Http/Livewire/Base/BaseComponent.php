<?php


namespace App\Http\Livewire\Base;


use App\Http\Livewire\Base\Datatable\WithBulkActions;
use App\Http\Livewire\Base\Datatable\WithCachedRecords;
use App\Http\Livewire\Base\Datatable\WithCustomPagination;
use App\Http\Livewire\Base\Datatable\WithSorting;
use App\Models\Freelancer\Freelancer;
use Livewire\Component;
use Livewire\WithPagination;

class BaseComponent extends Component
{
    use WithCustomPagination, WithSorting, WithBulkActions, WithCachedRecords;

    public bool $showEditModal = false;
    public bool $showDeleteModal = false;

    public bool $showFilters = false;


    protected $queryString = ['sorts'];


    public function createBlankModel($model)
    {
        $this->record = $model::make();
    }

    public function getRecordsProperty() //returns all paginated results
    {
        return $this->cache(function () {
            return $this->applyPagination($this->recordsQuery);
        });
    }


    public function resetFilters()
    {
        $this->reset('filters');
    }


    public function toggleShowFilters()
    {
        $this->useCachedRecords();
        $this->showFilters = !$this->showFilters;
    }

    //resets to page 1 once a filter is applied
    public function updatedFilters()
    {
        $this->resetPage();
    }

    public function deleteSelected() //delete bulk records
    {
        $this->selectedRecordsQuery->delete();
        $this->showDeleteModal = false;
    }


    public function save()
    {

        $this->validate();
        $this->record->save();
        $this->showEditModal = false;
    }


    public function saveFreelancerEntry($relationship)
    {
        $applicant = Freelancer::getThisFreelancer();
        $this->validate();
        $applicant->$relationship()->save($this->record);
        $this->showEditModal = false;
        $this->dispatchBrowserEvent('notify', 'Record Saved!');
    }

    public function getFreelancersBySkill($id)
    {
        $this->filters['skill_id'] = $id;
        request()->session()->flash('filters',  $this->filters);
        return redirect()->route('freelancers');
    }




}
