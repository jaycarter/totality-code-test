<?php

namespace App\Http\Livewire\Base\Datatable;

use Livewire\WithPagination;

trait WithCustomPagination
{
    use WithPagination;

    public $perPage = 15;


    public function initializeWithCustomPagination()
    {
        $this->perPage = session()->get('perPage', $this->perPage);
    }

    public function updatedPerPage($value) //store perPage value in user session
    {
        session()->put('perPage', $value);
    }

    public function applyPagination($query)
    {
        return $query->paginate($this->perPage);

    }
}
