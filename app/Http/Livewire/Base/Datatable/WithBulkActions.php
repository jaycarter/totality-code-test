<?php

namespace App\Http\Livewire\Base\Datatable;

trait WithBulkActions
{
    public array $selected = [];
    public bool $selectPage = false;
    public bool $selectAll = false;

    public function renderingWithBulkActions()
    {
        if ($this->selectAll) $this->selectPageRecords();
    }

    public function updatedSelected()
    {
        $this->selectAll = false;
        $this->selectPage = false;
    }


    public function updatedSelectPage($value) //Selects all on a page
    {
        if ($value) return $this->selectPageRecords();
        $this->selected = [];
    }

    public function selectAll()
    {
        $this->selectAll = true;
    }


    public function selectPageRecords()
    {
        $this->selected = $this->records->pluck('id')->map(fn($id) => (string)$id)->toArray();
    }

    public function exportSelected($fileName = 'records.csv')
    {
        return response()->streamDownload(function () {
            echo $this->selectedRecordsQuery->toCsv();
        }, $fileName);
    }

    public function deleteSelected()
    {
        $this->selectedRecordsQuery->delete();
        $this->showDeleteModal = false;
    }

    public function getSelectedRecordsQueryProperty()
    {
        return (clone $this->recordsQuery)
            ->unless($this->selectAll, fn($q) => $q->whereKey($this->selected));
    }

}
