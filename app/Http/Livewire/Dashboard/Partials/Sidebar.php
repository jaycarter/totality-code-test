<?php

namespace App\Http\Livewire\Dashboard\Partials;

use Livewire\Component;

class Sidebar extends Component
{
    public function render()
    {
        return view('livewire.dashboard.partials.sidebar');
    }
}
