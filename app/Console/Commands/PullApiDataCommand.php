<?php

namespace App\Console\Commands;

use App\Models\Appearance;
use App\Models\Category;
use App\Models\Character;
use App\Models\Occupation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class PullApiDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:pullData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull data from Breaking Bad API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->getDataFromApi();
    }


    private function getDataFromApi()
    {
        $url = 'https://www.breakingbadapi.com/api/characters';
        $response = Http::get($url);

        if ($response->successful()) {
            $characters = json_decode($response->body());

            foreach ($characters as $character) {
                $record = new Character();
                $record->char_id = $character->char_id;
                $record->name = $character->name;
                $record->birthday = $character->birthday;
                $record->img = $character->img;
                $record->status = $character->status;
                $record->nickname = $character->nickname;
                $record->portrayed = $character->portrayed;
                $record->save();

                if (count($character->occupation) > 0) {
                    foreach ($character->occupation as $occupation) {
                        $oRecord = new Occupation();
                        $oRecord->character_id = $record->id;
                        $oRecord->occupation = $occupation;
                        $oRecord->save();
                    }
                }

                if (count($character->appearance) > 0) {
                    foreach ($character->appearance as $appearance) {
                        $aRecord = new Appearance();
                        $aRecord->character_id = $record->id;
                        $aRecord->episode = $appearance;
                        $aRecord->save();
                    }
                }

                if ($character->category) {
                    foreach (explode(',', $character->category) as $cat) { // explode and loop
                        $cRecord = new Category();
                        $cRecord->character_id = $record->id;
                        $cRecord->category = Str::of($cat)->trim();
                        $cRecord->save();
                    }
                }
            }
            $this->info('Data Imported Successfully!');
        } else {
            $this->info('Error Importing Data!');
        }
    }
}
