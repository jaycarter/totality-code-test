<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['character_id', 'category'];

    public function Character()
    {
        return $this->belongsTo(Character::class);
    }

}
