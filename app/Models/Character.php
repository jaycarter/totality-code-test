<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    use HasFactory;

    public $fillable = ['char_id', 'name', 'birthday', 'img', 'status', 'nickname', 'portrayed'];



    public function occupations()
    {
        return $this->hasMany(Occupation::class);
    }

    public function appearances()
    {
        return $this->hasMany(Appearance::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }



}
