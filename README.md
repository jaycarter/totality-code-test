- Install Project
- Run `php artisan migrate:fresh --seed`
- Log in with the follwing credentials:
    username: admin@demo.com
    password: password
- Run `php artisan command:pullData` to fetch initial data from API
- Run `php artisan serve`
