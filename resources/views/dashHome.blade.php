@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')


    <div>


        <div x-data="{ sidebarOpen: false }" class="flex h-screen bg-gray-200">
            <div :class="sidebarOpen ? 'block' : 'hidden'" @click="sidebarOpen = false"
                 class="fixed z-20 inset-0 bg-black opacity-50 transition-opacity lg:hidden"></div>


            <div :class="sidebarOpen ? 'translate-x-0 ease-out' : '-translate-x-full ease-in'"
                 class="fixed z-30 inset-y-0 left-0 w-64 transition duration-300 transform bg-red-900  lg:translate-x-0 lg:static lg:inset-0">
                <div class="flex items-center justify-center mt-8">
                    <div class="flex items-center">
                        <a href="{{ route('home') }}">
                            <img class="mx-auto h-12 w-auto" src="/assets/white_logo.png" alt="white_logo">
                        </a>
                    </div>
                </div>
                @livewire('dashboard.partials.sidebar')

            </div>


            <div class="flex-1 flex flex-col overflow-auto">

                @livewire('dashboard.partials.header')
                <x-notification/>


                <div class="h-full overflow-auto ">

                    @yield('dashBody')
                </div>
            </div>
        </div>
    </div>

@endsection
