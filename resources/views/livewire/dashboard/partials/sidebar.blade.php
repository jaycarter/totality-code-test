<nav class="mt-10">


    <div class="flex flex-col items-center mb-6 mt-6 -mx-2">

        <img class="object-cover w-24 h-24 mx-2 rounded-full"
             src="https://images.unsplash.com/photo-1554151228-14d9def656e4?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=686&q=80"
             alt="avatar">


        <a href="#">
            <h4 class="mx-2 mt-2 font-semibold text-white dark:text-white hover:underline">
                John Doe
            </h4>
        </a>


        <p class="mx-2 mt-1 mb-3 text-sm text-white dark:text-white ">
            johndoe@gmail.com</p>

    </div>


    {{-- DASHBOARD --}}
    <div class="">
        <a href="{{ route('dashHome') }}"
           class="flex px-6  mt-6 items-center p-2 text-gray-300 transition-colors hover:bg-red-700" role="button">


            <span aria-hidden="true">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                     stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M16 8v8m-4-5v5m-4-2v2m-2 4h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                </svg>

            </span>
            <span class="ml-2 text-sm"> Dashboard </span>

        </a>
    </div>
    {{-- END OF DASHBOARD --}}





















    <!--LOG OUT -->
    <div>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
           class="flex px-6  items-center p-2 pt-3 text-gray-300 transition-colors dark:text-light hover:bg-red-700"
           role="button">


            <span aria-hidden="true">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" class="w-5 h-5"
                     stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
                </svg>
            </span>
            <span class="ml-2 text-sm"> Log Out </span>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

        </a>
    </div>


</nav>
