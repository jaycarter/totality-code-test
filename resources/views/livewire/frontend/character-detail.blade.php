<div class="bg-gray-100  p-5">

    <!-- End of Navbar -->

    <div class="container mx-auto ">


        <div class="md:flex no-wrap md:-mx-2 ">
            <!-- Left Side -->
            <div class="w-full md:w-3/12 md:mx-2">
                <!-- Card -->
                <div class="bg-white p-3 border-t-4 border-green-400">
                    <div class="image overflow-hidden">
                        <img class="h-auto w-full mx-auto"
                             src="{{ $record->img }}"
                             alt="{{ $record->img }}_profile_photo">
                    </div>
                    <h1 class="text-center text-gray-900 font-bold text-xl leading-8 mt-1 -mb-2">{{ $record->name }} </h1>


                    <div class="mx-auto text-sm text-center font-semibold my-6">
                        @foreach ($record->categories as $cat)
                            <span class="text-green-700">{{ $cat->category }}  @if(!$loop->last) - @endif </span>
                        @endforeach
                    </div>

                    <p class="text-sm pt-3 text-gray-500 hover:text-gray-600 leading-6"> {!! $record->about_me !!}</p>
                    <ul
                        class="bg-gray-100 text-gray-600 hover:text-gray-700 hover:shadow py-2 px-3 mt-3 divide-y rounded shadow-sm">
                        <li class="flex items-center py-3">
                            <span>Status</span>
                            @if($record->status == "Alive")
                                <span class="ml-auto"><span
                                        class="bg-green-500 py-1 px-2 rounded text-white text-sm">{{$record->status}}</span></span>
                            @else
                                <span class="ml-auto"><span
                                        class="bg-red-500 py-1 px-2 rounded text-white text-sm">{{$record->status}}</span></span>
                            @endif
                        </li>
                        <li class="flex items-center py-3">
                            <span>Created at</span>
                            <span
                                class="ml-auto"> {{\Carbon\Carbon::parse($record->created_at)->format('d M Y')}}</span>
                        </li>

                        @auth
                            <li class="flex items-center py-3">
                                <x-button.link wire:click="edit({{$record->id}})">
                                    <x-icon.edit/>
                                    Edit Character Profile
                                </x-button.link>


                            </li>
                        @endauth


                    </ul>
                </div>
                <!-- End of  card -->
                <div class="my-4"></div>

                <!--  Card -->
                @if(!$record->occupations->isEmpty())
                    <div class="bg-white my-3 p-3 space-y-2 hover:shadow">
                        <div class="flex items-center space-x-3 font-semibold text-gray-900 text-xl leading-8">
                                <span class="text-green-500">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                                         fill="currentColor">
                                        <path
                                            d="M2 11a1 1 0 011-1h2a1 1 0 011 1v5a1 1 0 01-1 1H3a1 1 0 01-1-1v-5zM8 7a1 1 0 011-1h2a1 1 0 011 1v9a1 1 0 01-1 1H9a1 1 0 01-1-1V7zM14 4a1 1 0 011-1h2a1 1 0 011 1v12a1 1 0 01-1 1h-2a1 1 0 01-1-1V4z"/>
                                      </svg>
                                </span>
                            <span>Occupations</span>
                        </div>

                        <div class="flex flex-col pl-8 space-y-1">
                            <ul class="list-disc">
                                @foreach ($record->occupations as $occu)
                                    <div class="flex">
                                        <div class="ml-3">
                                            <li class=" text-md">{{ $occu->occupation }}</li>
                                        </div>
                                    </div>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif


                @if(!$record->appearances->isEmpty())
                    <div class="bg-white my-3 p-3 space-y-2 hover:shadow">
                        <div class="flex items-center space-x-3 font-semibold text-gray-900 text-xl leading-8">
                                <span class="text-green-500">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path d="M12 14l9-5-9-5-9 5 9 5z"/>
                                        <path
                                            d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z"/>
                                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"/>
                                      </svg>
                                </span>
                            <span>Appearances</span>
                        </div>

                        <div class="flex flex-col pl-8 space-y-1">
                            <ul class="list-disc">
                                @foreach ($record->appearances as $app)
                                    <div class="flex">
                                        <div class="ml-3">
                                            <li class=" text-md">Season {{ $app->episode }}</li>
                                        </div>
                                    </div>
                                @endforeach
                            </ul>
                        </div>
                    </div>
            @endif





            <!-- End of  card -->
            </div>
            <!-- Right Side -->
            <div class="w-full md:w-9/12 mx-2 ">
                <div class="bg-white p-3 shadow-sm rounded-sm">
                    <div class="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
                           <span clas="text-green-500">
                               <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                    stroke="currentColor">
                                   <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                         d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"/>
                               </svg>
                           </span>
                        <span class="tracking-wide">About</span>
                    </div>
                    <div class="text-gray-700">
                        <div class="grid md:grid-cols-2 text-sm">
                            <div class="grid grid-cols-2">
                                <div class="px-4 py-2 font-semibold">Name:</div>
                                <div class="px-4 py-2">{{ $record->name }} </div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="px-4 py-2 font-semibold">Birthday:</div>
                                <div
                                    class="px-4 py-2">{{$record->birthday}}</div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="px-4 py-2 font-semibold">Status:</div>
                                <div class="px-4 py-2">{{$record->status}}
                                </div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="px-4 py-2 font-semibold">Nickname:</div>
                                <div class="px-4 py-2">{{ $record->nickname }}</div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="px-4 py-2 font-semibold">Portrayed By:</div>
                                <div class="px-4 py-2">{{ $record->portrayed }}</div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="my-4"></div>

                <!-- Death Info -->
                @if ($deathInfo != null)
                    <div class="bg-white p-3 shadow-sm  rounded-sm">
                        <div class="w-full overflow-hidden container mx-auto md:w-5/6">
                            <section class="text-gray-600 body-font">
                                <h1 class="mt-3 text-center md:text-3xl text-2xl font-semibold font-dosis  text-red-600">
                                    Death Information</h1>
                                <div class="container px-5 py-12 mx-auto">
                                    {{--                                    @json($deathInfo)--}}
                                    <div class="flex  flex-wrap -mx-4 -mb-10 text-left">
                                        <div class="grid md:grid-cols-1 text-sm">
                                            <div class="grid grid-cols-2">
                                                <div class="px-4 py-2 font-semibold">Cause of death:</div>
                                                <div class="px-4 py-2">{{ $deathInfo[0]->cause }} </div>
                                            </div>
                                            <div class="grid grid-cols-2">
                                                <div class="px-4 py-2 font-semibold">Responsible:</div>
                                                <div class="px-4 py-2">{{ $deathInfo[0]->responsible }} </div>
                                            </div>
                                            <div class="grid grid-cols-2">
                                                <div class="px-4 py-2 font-semibold">Last Words:</div>
                                                <div class="px-4 py-2">{{ $deathInfo[0]->last_words }} </div>
                                            </div>
                                            <div class="grid grid-cols-2">
                                                <div class="px-4 py-2 font-semibold">When:</div>
                                                <div class="px-4 py-2">Season {{ $deathInfo[0]->season }},
                                                    Episode {{ $deathInfo[0]->episode }} </div>
                                            </div>
                                            <div class="grid grid-cols-2">
                                                <div class="px-4 py-2 font-semibold">Number of Deaths:</div>
                                                <div class="px-4 py-2">{{ $deathInfo[0]->number_of_deaths }} </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                @else
                    <h1 class="my-6 text-center md:text-3xl text-2xl font-semibold font-dosis  text-green-600">
                        {{$record-> name }} is Still Alive</h1>
                @endif


                @if ($quotes != null)
                    <div class="bg-white p-3 shadow-sm  rounded-sm">
                        <div class="w-full overflow-hidden container mx-auto md:w-5/6">
                            <section class="text-gray-600 body-font">
                                <h1 class="mt-3 text-center md:text-3xl text-2xl font-semibold font-dosis  text-gray-500">
                                    Quotes</h1>
                                <div class="container px-5 pb-12 mx-auto">
                                @foreach($quotes as $q)
                                    <!-- component -->
                                        <div class="max-w-4xl p-4 my-6 text-gray-800 bg-gray-50 rounded-lg shadow">
                                            <div class="mb-2">
                                                <div class="h-3 text-4xl text-left text-green-600">“</div>
                                                <p class="px-4 text-sm text-center text-gray-600">
                                                    {{$q->quote}}
                                                </p>
                                                <div class="h-3 text-4xl text-right text-green-600">”</div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </section>
                        </div>
                    </div>
                @else
                    <h1 class="mt-3 text-center md:text-3xl text-2xl font-semibold font-dosis  text-green-600">
                        Still Alive</h1>
                @endif

            </div>
        </div>
    </div>

    <form wire:submit.prevent="save">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">{{$record->name}}
            </x-slot>
            {{--        @dd($record)--}}

            <x-slot name="content">

                <div class="my-2 px-2 w-full overflow-hidden xl:w-full">
                    <label for="location" class="block text-sm font-medium text-gray-700">Name*</label>
                    <input wire:model.defer="record.name" type="text" name="name" id="name"
                           autocomplete="location" placeholder="Add Category"
                           class="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    @error('record.name')
                    <p class="mt-2 text-sm text-red-600">Name is required</p>
                    @enderror
                </div>


                <div class="my-2 px-2 w-full overflow-hidden xl:w-full">
                    <label for="nickname" class="block text-sm font-medium text-gray-700">Status*</label>
                    <input wire:model.defer="record.status" type="text" name="status" id="status"
                           autocomplete="status" placeholder="status"
                           class="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    @error('record.status')
                    <p class="mt-2 text-sm text-red-600">Status is required</p>
                    @enderror
                </div>

                <div class="my-2 px-2 w-full overflow-hidden xl:w-full">
                    <label for="nickname" class="block text-sm font-medium text-gray-700">Nickname*</label>
                    <input wire:model.defer="record.nickname" type="text" name="nickname" id="nickname"
                           autocomplete="nickname" placeholder="nickname"
                           class="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    @error('record.nickname')
                    <p class="mt-2 text-sm text-red-600">Nickname is required</p>
                    @enderror
                </div>


            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showEditModal',false)">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>

            </x-slot>
        </x-modal.dialog>
    </form>

</div>
