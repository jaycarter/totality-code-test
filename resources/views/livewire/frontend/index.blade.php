<div>
    <header>
        <div class="w-full bg-center bg-cover h-128"
             style="background-image: url(http://blog.wordnik.com/wp-content/uploads/breaking_bad1.jpeg);">
            <div class="flex items-center justify-center w-full h-full bg-gray-900 bg-opacity-80">
                <div class="text-center py-28">
                    <h1 class="text-3xl pb-3 font-semibold text-white uppercase lg:text-4xl">Find your favorite <span
                            class="text-green-500 ">Breaking Bad</span> Character</h1>

                    <div class=" flex flex-wrap overflow-hidden xl:-mx-3">


                        <div class="w-full pb-3 overflow-hidden xl:my-3 xl:px-3 xl:w-1/3">
                            <input type="text" wire:model="filters.search"
                                   class="w-full pl-3 pr-10 py-2 border-2 border-gray-200  hover:border-gray-300
                                focus:outline-none focus:border-blue-500 transition-colors"
                                   placeholder="Search by Character Name">
                        </div>


                        <div class="w-full pb-3 overflow-hidden xl:my-3 xl:px-3 xl:w-1/3">

                            <select type="text" wire:model="filters.category"
                                    class="w-full pl-3 pr-10 py-2 border-2 border-gray-200  hover:border-gray-300
                                focus:outline-none focus:border-blue-500 transition-colors" placeholder="">
                                <option value="">Select Category</option>
                                <option value="Breaking Bad">Breaking Bad</option>
                                <option value="Better Call Saul">Better Call Saul</option>

                            </select>
                        </div>

                        <div class="w-full pb-2 overflow-hidden xl:my-3 xl:px-3 xl:w-1/3">
                            <select type="text" wire:model="filters.season"
                                    class="w-full pl-3 pr-10 py-2 border-2 border-gray-200  hover:border-gray-300
                                focus:outline-none focus:border-blue-500 transition-colors" placeholder="">
                                <option value="">Select Season</option>
                                <option value=1>1</option>
                                <option value=2>2</option>
                                <option value=3>3</option>
                                <option value=4>4</option>
                                <option value=4>5</option>

                            </select>
                        </div>
                    </div>


                    <div class="relative h-6">
                        @if($filters['search'] || $filters['category'] || $filters['season']  )
                            <p wire:click="clearFilters" class="pt-2 cursor-pointer text-white text-sm underline">Clear
                                Filters</p>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </header>

    <div>
        <div class="h-92 mx-auto w-full flex flex-wrap overflow-hidden  xl:my-6 xl:px-3 xl:w-full">

            @forelse ($characters as $character)
                <div class="mx-auto">
                    {{--                    @json($character)--}}

                    <div class="my-3 mx-9 max-w-sm  overflow-hidden bg-white rounded-lg shadow-lg dark:bg-gray-800">
                        @if ($character->img == null)
                            <img class="object-cover object-center w-full h-56"
                                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSE3tkpY8dQsvNnaCAbZeNpH6uNfavAcbuFNQ&usqp=CAU"
                                 alt="avatar">
                        @else
                            <img class="object-cover object-center w-full h-56"
                                 src="{{$character->img  }} " alt="avatar">
                        @endif


                        <div class=" text-center items-center px-6 py-3 bg-gray-900">

                            <a href="{{route('characterDetail', $character->id)}}">
                                <h1 class="mx-3 text-center text-lg font-semibold text-white">{{ $character->name }}</h1>
                            </a>
                        </div>

                    </div>

                </div>


            @empty
                <div class=" items-center justify-center w-full text-center ">
                    <p class="text-md md:text-3xl font-semibold">No Characters Found</p>
                </div>
            @endforelse

            <div class="container my-12">
                {{ $characters->links() }}
            </div>

        </div>


    </div>

</div>
