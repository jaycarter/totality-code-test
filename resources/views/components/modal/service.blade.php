@props(['id' => null, 'maxWidth' => null])

<x-modal :id="$id" :maxWidth="$maxWidth" {{ $attributes }}>
    <div class="px-6 py-4 max-h-full ">
        <div class="text-lg">
            {{ $title }}
        </div>

        <div class="mt-4 flex flex-col 	">
            {{ $content }}
        </div>
    </div>

    <div class="px-6 py-4  text-right">
        {{ $footer }}
    </div>
</x-modal>


