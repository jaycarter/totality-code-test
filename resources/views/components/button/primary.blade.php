@props([
'style'
])

@php
    $style = $style ?? ''
@endphp


<x-button {{ $attributes->merge(['class' => 'text-white bg-green-600 hover:bg-green-500 active:bg-green-700 border-green-600 ' . ($style ? $style : '')  ]) }}>{{ $slot }}</x-button>
