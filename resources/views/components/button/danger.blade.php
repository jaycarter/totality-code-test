@props([
'style'
])

@php
    $style = $style ?? ''
@endphp


<x-button {{ $attributes->merge(['class' => 'text-white bg-red-600 hover:bg-blue-500 active:bg-red-700 border-red-600 ' . ($style ? $style : '')  ]) }}>{{ $slot }}</x-button>
